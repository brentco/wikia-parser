package com.runelocus.parser;

import com.google.gson.Gson;
import com.runelocus.parser.impl.DiskCachingPageProvider;
import com.runelocus.parser.wikia.DetailsPage;
import com.runelocus.parser.wikia.InfoboxRow;
import com.runelocus.parser.wikia.WikiaPage;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

public class PageTest {

    private PageProvider<WikiaPage> provider = new DiskCachingPageProvider<>(WikiaPage.class);

    @Test
    public void whatever() throws Exception {

        Optional<DetailsPage> page = provider.provide("http://runescape.wikia.com/wiki/Man");

        page.ifPresent(p -> {
            List<InfoboxRow> els = p.getInfoBox().getRows();
            els.forEach(e -> e.getValue().stream().map(new Gson()::toJson).forEach(System.out::println));
        });
    }

}