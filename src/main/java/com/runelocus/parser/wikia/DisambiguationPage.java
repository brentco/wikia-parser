package com.runelocus.parser.wikia;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class DisambiguationPage extends WikiaPage {

    public DisambiguationPage(Document document) {
        super(document);
    }

    @Override
    protected void parse() {
        Elements el = getDocument().getElementsByAttributeValueContaining("class", "disambig");
        if (el.size() == 0) {
            return;
        }
        triggerSuccess();
    }
}
