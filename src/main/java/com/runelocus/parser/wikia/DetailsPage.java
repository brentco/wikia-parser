package com.runelocus.parser.wikia;

import org.jsoup.nodes.Document;

public class DetailsPage extends WikiaPage {

    private InfoBox infoBox;

    public DetailsPage(Document document) {
        super(document);
    }

    @Override
    protected void parse() {
        infoBox = new InfoBox(this);
        if (infoBox.isNotSuccess()) {
            return;
        }

        triggerSuccess();
    }

    public InfoBox getInfoBox() {
        return infoBox;
    }
}
