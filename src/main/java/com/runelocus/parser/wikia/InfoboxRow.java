package com.runelocus.parser.wikia;

import com.runelocus.parser.PageElement;
import com.runelocus.parser.wikia.rows.RowParser;
import com.runelocus.parser.wikia.rows.beans.Bean;
import com.runelocus.parser.wikia.rows.parsers.ReleaseRowParser;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class InfoboxRow extends PageElement<DetailsPage> {

    private static final RowParser[] ROW_PARSERS = {new ReleaseRowParser()};
    private Element tableRow;

    private Element name;

    private List<Bean> value = new ArrayList<>();

    public InfoboxRow(DetailsPage parent, Element tableRow) {
        super(parent);
        this.tableRow = tableRow;
        loadOwn();
    }

    @Override
    public void loadFromDocument(Document element) {

    }

    private void loadOwn() {
        name = tableRow.getElementsByTag("th").first();


        Elements value = tableRow.getElementsByTag("td");
        if(value.size() == 0) {
            return;
        }

//        Elements valueContents = value.first().getAllElements();
        if(name != null) {
            System.out.println("Parsing " + name.text());
        }
        for(RowParser parser : ROW_PARSERS) {
            if(parser.match(name, value.first())) {
                this.value.add(parser.process(name, value.first()));
                break;
            }
        }

        triggerSuccess();
    }

    public Element getName() {
        return name;
    }

    public List<Bean> getValue() {
        return value;
    }
}
