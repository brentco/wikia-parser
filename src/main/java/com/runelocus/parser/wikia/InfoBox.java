package com.runelocus.parser.wikia;

import com.runelocus.parser.PageElement;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class InfoBox extends PageElement<DetailsPage> {

    private Element caption;

    private List<InfoboxRow> rows = new ArrayList<>();

    public InfoBox(DetailsPage parent) {
        super(parent);
        rows = new ArrayList<>();
        loadOwn(getParent().getDocument());
    }

    private void loadOwn(Document doc) {
        Element contentElement = doc.getElementById("mw-content-text");
        if (contentElement == null) {
            return;
        }

        Elements tableCandidates = contentElement.getElementsByAttributeValueContaining("class", "wikitable infobox");
        if (tableCandidates.isEmpty()) {
            return;
        }

        Element infoTable = tableCandidates.first();
        if (infoTable == null) {
            return;
        }

        Elements captionElement = infoTable.getElementsByTag("caption");
        if (captionElement.size() > 0) {
            caption = captionElement.first();
        }

        Element tableBody = infoTable.getElementsByTag("tbody").first();


        for (Element row : tableBody.getElementsByTag("tr")) {
            try {
                rows.add(new InfoboxRow(getParent(), row));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        triggerSuccess();
    }

    @Override
    public void loadFromDocument(Document doc) {

    }

    public Element getCaption() {
        return caption;
    }

    public List<InfoboxRow> getRows() {
        return rows;
    }
}
