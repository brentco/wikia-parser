package com.runelocus.parser.wikia;

import com.runelocus.parser.Page;
import org.jsoup.nodes.Document;

public class WikiaPage extends Page {

    protected WikiaPage(Document document) {
        super(document);
    }

    public static WikiaPage create(Document doc) {
        DetailsPage dp = new DetailsPage(doc);
        if (dp.isSuccess()) {
            return dp;
        }

        DisambiguationPage dip = new DisambiguationPage(doc);
        if (dip.isSuccess()) {
            return dip;
        }

        return null;
    }
}
