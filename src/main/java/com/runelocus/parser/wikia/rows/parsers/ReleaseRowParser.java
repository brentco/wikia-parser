package com.runelocus.parser.wikia.rows.parsers;

import com.runelocus.parser.wikia.rows.RowParser;
import com.runelocus.parser.wikia.rows.beans.Bean;
import com.runelocus.parser.wikia.rows.beans.ReleaseBean;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ReleaseRowParser implements RowParser<ReleaseBean> {

    @Override
    public boolean match(Element th, Element td) {
        return th != null && th.text().trim().equals("Release date");
    }

    @Override
    public ReleaseBean process(Element th, Element td) {
        String name = th.text().trim();
        String dateMonth = null, year = null, updateText = null, updateLink = null;
        Elements anchors = td.getElementsByTag("a");
        if(anchors.size() == 3) {
            dateMonth = anchors.get(0).text().trim();
            year = anchors.get(1).text().trim();
            updateLink = anchors.get(2).attr("href");
            updateText = anchors.get(2).attr("title");
        }

        return new ReleaseBean(name, dateMonth, year, updateLink, updateText);
    }
}
