package com.runelocus.parser.wikia.rows.beans;

public class ReleaseBean extends SimpleTextBean {
    private String dateMonth;
    private String year;
    private String updateLink;
    private String updateText;

    public ReleaseBean(String name, String dateMonth, String year, String updateLink, String updateText) {
        super(name, updateText);
        this.dateMonth = dateMonth;
        this.year = year;
        this.updateLink = updateLink;
        this.updateText = updateText;
    }

    public String getDateMonth() {
        return dateMonth;
    }

    public String getYear() {
        return year;
    }

    public String getUpdateLink() {
        return updateLink;
    }

    public String getUpdateText() {
        return updateText;
    }
}
