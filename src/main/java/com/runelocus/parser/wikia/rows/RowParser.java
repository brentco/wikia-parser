package com.runelocus.parser.wikia.rows;

import com.runelocus.parser.wikia.rows.beans.Bean;
import org.jsoup.nodes.Element;

public interface RowParser<E extends Bean> {
    boolean match(Element th, Element td);
    E process(Element th, Element td);
}
