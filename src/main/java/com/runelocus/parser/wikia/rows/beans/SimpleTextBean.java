package com.runelocus.parser.wikia.rows.beans;

public class SimpleTextBean implements Bean {
    private String name;
    private String text;

    public SimpleTextBean(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }
}
