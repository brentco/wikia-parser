package com.runelocus.parser;

import org.jsoup.nodes.Document;

public abstract class PageElement<E extends Page> {

    private E parent;

    private boolean success;

    public PageElement(E parent) {
        this.parent = parent;
        loadFromDocument(parent.getDocument());
    }

    public abstract void loadFromDocument(Document element);

    public E getParent() {
        return parent;
    }

    protected void triggerSuccess() {
        success = true;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isNotSuccess() {
        return !success;
    }
}
