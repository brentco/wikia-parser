package com.runelocus.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

public class Page {

    protected Logger logger = LogManager.getLogger(this);
    private Document document;
    private boolean success;

    public Page(Document document) {
        this.document = document;
        parse();
    }

    protected void parse() {

    }

    public Document getDocument() {
        return document;
    }

    public boolean isSuccess() {
        return success;
    }

    public boolean isNotSuccess() {
        return !success;
    }

    protected void triggerSuccess() {
        success = true;
    }
}
