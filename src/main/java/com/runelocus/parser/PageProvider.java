package com.runelocus.parser;

import java.util.Optional;

public interface PageProvider<E extends Page> {
    <T extends E> Optional<T> provide(String url);
}
