package com.runelocus.parser.impl;

import com.runelocus.parser.Page;
import com.runelocus.parser.PageProvider;
import com.runelocus.parser.util.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Optional;

public class DiskCachingPageProvider<E extends Page> implements PageProvider<E> {

    private static final Path basePath = Paths.get("pageCache");
    private Logger logger = LogManager.getLogger(this);

    private Constructor<E> constructor;
    private Class<E> type;
    private Method factoryMethod;

    public DiskCachingPageProvider(Class<E> type) {
        if (Files.notExists(basePath)) {
            try {
                Files.createDirectory(basePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.type = type;
        try {
            constructor = type.getConstructor(Document.class);
        } catch (NoSuchMethodException e) {
        }

        for (Method method : type.getDeclaredMethods()) {
            if (method.getName().equals("create") && (method.getModifiers() & Modifier.STATIC) != 0) {
                factoryMethod = method;
                break;
            }
        }

        if(constructor == null && factoryMethod == null) {
            logger.error("Page type '{}' does not have the required constructor(Document) or static create(Document) method!", type.getSimpleName());
        }
    }

    private Optional<E> invokeConstructor(Document doc) {
        if (doc == null) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(constructor.newInstance(doc));
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            logger.error("Unable to instantiate '{}': {}", type.getSimpleName(), e.getMessage());
        }

        return Optional.empty();
    }

    private Optional<E> invoke(Document document) {
        if (factoryMethod == null) {
            return invokeConstructor(document);
        }

        try {
            return Optional.ofNullable((E) factoryMethod.invoke(null, document));
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error("Unable to instantiate '{}': {}", type.getSimpleName(), e.getMessage());
        }

        return Optional.empty();
    }

    @Override
    public Optional<E> provide(String url) {

        Document doc = null;

        if (isCached(url)) {
            Optional<Document> documentOptional = getFromCache(url);
            if (documentOptional.isPresent()) {
                doc = documentOptional.get();
            }
        } else {
            try {
                doc = Jsoup.connect(url).timeout(10000).get();
                cachePage(doc);
            } catch (IOException e) {
                logger.error("Unable to retrieve '{}': {}", url, e.getMessage());
            }
        }

        return invoke(doc);
    }

    private void cachePage(Document document) throws IOException {
        logger.debug("Caching '{}'", document.baseUri());

        String hash = Utils.urlToHash(document.baseUri());
        Path filePath = basePath.resolve(hash + ".cdo");
        Files.write(filePath, document.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
    }

    public Optional<Document> getFromCache(String url) {
        logger.debug("Retrieving '{}' from cache", url);
        String hash = Utils.urlToHash(url);
        Path filePath = basePath.resolve(hash + ".cdo");

        if (Files.notExists(filePath)) {
            return Optional.empty();
        }

        try {
            return Optional.ofNullable(Jsoup.parse(filePath.toFile(), "UTF-8", url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static boolean isCached(String url) {
        String hash = Utils.urlToHash(url);
        Path filePath = basePath.resolve(hash + ".cdo");
        return Files.exists(filePath);
    }

    public void invalidate(String url) {
        String hash = Utils.urlToHash(url);
        Path filePath = basePath.resolve(hash + ".cdo");
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException e) {
            logger.error("Unable to delete '{}': {}", filePath, e.getMessage());
        }
    }
}
