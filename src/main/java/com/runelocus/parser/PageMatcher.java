package com.runelocus.parser;

import org.jsoup.nodes.Document;

public interface PageMatcher {
    boolean hasRequiredElements(Document document);
}
