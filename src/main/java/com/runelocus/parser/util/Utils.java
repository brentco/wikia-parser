package com.runelocus.parser.util;

import org.apache.commons.codec.binary.Base32;

public class Utils {
    public static String urlToHash(String url) {
        Base32 base32 = new Base32(true);
        return base32.encodeToString(url.getBytes());
    }

    public static String hashToUrl(String hash) {
        Base32 base32 = new Base32(true);
        return new String(base32.decode(hash.getBytes()));
    }
}
